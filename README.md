# Sword's Fire System V2.0

Featres:

  Weapons:
  
    Fire Extinguisher: Extinguishes Fires Slowly, Limited Fuel, Can Refil At Fire Truck / Fire Hydrant / Lake
    
    Fire Hose:         Extinguishes Fires Quickly, Infinite Fuel, Can ONLY Use Near Fire Truck / Fire Hydrant
    
    Fire Axe:          Unlimted Uses, Breaks Down Doors Near Fire
    
  Entities:
  
    Fire:              Deadly Fire That Damages Players / Entities, Spreads Rapidly And Can Be Extinguished
    
    Detector:          A Detector That Detects Fires, Will Alert The Owner And Fire Department
    
    Alarm:             An Alarm That Will Go Off When Fire Alerts Come To The Fire Station
    
    Hydrant:           An Entity That Allows You To Use Fire Hose / Refill Fire Extinguisher
    
  Jobs:
  
    Fire Fighter:      A DarkRP Job That Gives Players All The Weapons In This Addon On Spawn
  
  Gameplay:
  
    Random Fires:      Random Fires Start At Certain Location And With Specific Reasons
                       Eg: Due To A Man Inserting His Tie Into An ATM - The Fire Will Spawn At The ATM
    
  Config:
    
    Config:            Developer Can Change What Materials Catch Fire, What Vehicles Are Fire Trucks And More