function EFFECT:Init( data )

	local trace = {}
	trace.start = data:GetOrigin() - Vector(0, 0, 5)
	trace.endpos = data:GetOrigin() - Vector(0, 0, 5) + Vector(0, 0, 500)
	local tr = util.TraceLine( trace )
	
	local TheFire = GLOBAL_EMITTER:Add( "effects/flame", data:GetOrigin() - Vector(0, 0, 5))
	if tr.Hit then
		TheFire:SetVelocity( Vector( math.random(-30,30), math.random(-30, 30), math.random(0, 70) ) )
	else
		TheFire:SetVelocity( Vector( math.random(-30,-20), math.random(20, 30), math.random(0, 70) ) )
	end
	TheFire:SetDieTime( math.random( 2, 3 ) )
	TheFire:SetStartAlpha( 230 )
	TheFire:SetEndAlpha( 0 )
	TheFire:SetStartSize( math.random( 70, 80 ) )
	TheFire:SetEndSize( 10 )
	TheFire:SetRoll( math.random( 0, 10 ) )
	TheFire:SetRollDelta( math.random( -0.2, 0.2 ) )
	
	local SmokeEffect = GLOBAL_EMITTER:Add("particle/smokestack", data:GetOrigin() + Vector(0, 0, 50))
	SmokeEffect:SetAirResistance( 0 )
	SmokeEffect:SetVelocity( Vector(math.sin(CurTime() * 0.01) * 10, math.cos(CurTime() * 0.01) * 10, 100) + VectorRand() * 25 )
	SmokeEffect:SetDieTime( 12.5 )
	SmokeEffect:SetStartAlpha( math.random( 50, 150 ) )
	SmokeEffect:SetGravity( Vector( 5, 5, 3) )
	SmokeEffect:SetCollide( true )
	SmokeEffect:SetColor( Color( 100, 100, 100, 255 ) )
	SmokeEffect:SetEndAlpha( 0 )
	SmokeEffect:SetStartSize( math.random( 20, 30 ) )
	SmokeEffect:SetEndSize( math.random( 70, 160 ) )
	SmokeEffect:SetRoll( math.random( 0, 10 ) )
	SmokeEffect:SetRollDelta( math.random( -0.2, 0.2 ) )
	
	if tr.Hit and math.sin( CurTime() * 5 ) > 0 then
		local TheFire = GLOBAL_EMITTER:Add("effects/extinguisher", data:GetOrigin() - Vector(0, 0, 5) + Vector( math.random(-40,40), math.random(-40,40), math.random(50, 100) ) )
		TheFire:SetVelocity( Vector( math.random(-20, 20), math.random(-20, 20), math.random(0, 100) ) )
		TheFire:SetDieTime( 12.5 )
		TheFire:SetStartAlpha( 20 )
		TheFire:SetEndAlpha( 0 )
		TheFire:SetStartSize( math.random(20, 30) )
		TheFire:SetEndSize( 200 )
	end
end

function EFFECT:Render()
end