AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
	self:SetModel("models/props/de_nuke/emergency_lighta.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)

	self.AlarmPlaying = false

	local phys = self:GetPhysicsObject()

	if (phys:IsValid()) then
		phys:Wake()
	end
end

function ENT:SendMessage()
	if (sfs_DarkRPEnabled == true) then
	local Text = "A Fire Detector Has Gone Off! - Location Unknown"
	local Text2 = "One Of Your Fire Detectors Has Detected A Fire!"
	for k, v in pairs(player:GetAll()) do
		if(v:Team() == TEAM_FIREFIGHTER) then
			DarkRP.notify(v, 1, 4, Text)
		end
		if (v == self:GetSpawner()) then
			DarkRP.notify(v, 1, 4, Text2)
		end
	end
	end
end

function ENT:Think()
	local Alarm = false
	for k, v in pairs(ents.FindInSphere(self:GetPos(), 250)) do
		if (v:GetClass() == "fire") then
			Alarm = true
		end
	end
	if (Alarm == true and AlarmPlaying == false) then
		AlarmPlaying = true
		self:SendMessage()
	end
	if (Alarm == false) then
		AlarmPlaying = false
	end
end