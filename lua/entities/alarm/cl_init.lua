include("shared.lua")

function ENT:Draw()
	self:DrawModel()
end

function ENT:Initialize()	
	self.AlarmSound = CreateSound(self, Sound("ambient/alarms/alarm1.wav"))
	self.AlarmPlaying = false
end

function ENT:Think()
	local Alarm = false
	for k, v in pairs(ents.GetAll()) do
		if (v:GetClass() == "fire") then
			Alarm = true
		end
	end
	if (Alarm == true and AlarmPlaying == false) then
		AlarmPlaying = true
		self.AlarmSound:Play()
	end
	if (Alarm == false) then
		self.AlarmSound:Stop()
		AlarmPlaying = false
	end
end

function ENT:OnRemove()
	self.AlarmSound:Stop()
	AlarmPlaying = false
end