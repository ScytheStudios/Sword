function AddFireJob()
TEAM_FIREFIGHTER = AddExtraTeam("Fire Fighter", {
	color = Color(25, 25, 170, 255),
	model = "models/player/monk.mdl",
	description = [[Extinguish All Fires]],
	weapons = {"fire_extinguisher", "fire_axe", "fire_hose"},
	command = "firefighter",
	max = 3,
	salary = 250,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Civil Protection"
})
end
hook.Add("loadCustomDarkRPItems", "AddFireJobs", AddFireJob)