AddCSLuaFile()
AddCSLuaFile( "effects/extinguisher/init.lua" )

if ( SERVER ) then resource.AddWorkshop( "104607228" ) end

SWEP.PrintName = "Fire Hose"
SWEP.Author = "Sword"
SWEP.Category = "Sword's Fire System"
SWEP.Instructions = "Left Click To Extinguish Fire Rapidly"
SWEP.Slot = 2
SWEP.SlotPos = 2
SWEP.Weight = 1
SWEP.DrawAmmo = false
SWEP.FiresUnderwater = false
SWEP.AutoSwitchTo = true
SWEP.AutoSwitchFrom = true
SWEP.DrawWeaponInfoBox = false
SWEP.Spawnable = true
SWEP.AdminOnly = true
SWEP.UseHands = true
SWEP.ViewModel = "models/weapons/v_superphyscannon.mdl"
SWEP.ViewModelFOV = 55
SWEP.WorldModel = "models/weapons/w_superphyscannon.mdl"
SWEP.HoldType = "slam"

game.AddAmmoType( { name = "rb655_extinguish" } )
if ( CLIENT ) then language.Add( "rb655_extinguish_ammo", "Extinguisher Ammo" ) end
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = 5
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "rb655_extinguish"
SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

function SWEP:SetupDataTables()
	self:NetworkVar( "Float", 0, "NextIdle" )
end

function SWEP:Initialize()
	self:SetHoldType( self.HoldType )
end

function SWEP:Deploy()
	self:SendWeaponAnim( ACT_VM_DRAW )
	self:SetNextPrimaryFire( CurTime() + self:SequenceDuration() )
	
	self:Idle()

	return true
end

function SWEP:DoEffect( effectscale )

	local effectdata = EffectData()
	effectdata:SetAttachment( 1 )
	effectdata:SetEntity( self.Owner )
	effectdata:SetOrigin( self.Owner:GetShootPos() )
	effectdata:SetNormal( self.Owner:GetAimVector() )
	effectdata:SetScale( effectscale or 1 )
	util.Effect( "extinguisher", effectdata )

end

function SWEP:DoExtinguish( pushforce, effectscale )
	if ( self:Ammo1() < 1 ) then return end

	if ( CLIENT ) then
		if ( self.Owner == LocalPlayer() ) then self:DoEffect( effectscale ) end -- FIXME
		return
	end

	self:TakePrimaryAmmo( 1 )
	
	effectscale = effectscale or 1
	pushforce = pushforce or 0

	local tr
	if ( self.Owner:IsNPC() ) then
		tr = util.TraceLine( {
			start = self.Owner:GetShootPos(),
			endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * 16384,
			filter = self.Owner
		} )
	else
		tr = self.Owner:GetEyeTrace()
	end
	
	local pos = tr.HitPos

	for id, prop in pairs( ents.FindInSphere( pos, 80 ) ) do
		if ( prop:GetPos():Distance( self:GetPos() ) < 256 ) then
			if ( prop:IsValid() and math.random( 0, 1000 ) > 500 ) then
				if ( prop:IsOnFire() ) then prop:Extinguish() end

				local class = prop:GetClass()
				if ( string.find( class, "ent_minecraft_torch" ) and prop:GetWorking() ) then
					prop:SetWorking( false )
				elseif ( string.find( class, "env_fire" ) ) then -- Gas Can support. Should work in ravenholm too.
					prop:Fire( "Extinguish" )
				end
			end
			
			if ( pushforce > 0 ) then
				local physobj = prop:GetPhysicsObject()
				if ( IsValid( physobj ) ) then
					physobj:ApplyForceOffset( self.Owner:GetAimVector() * pushforce, pos )
				end
			end
		end
	end

	self:DoEffect( effectscale )

end

function SWEP:PrimaryAttack()
	if ( self:GetNextPrimaryFire() > CurTime() ) then return end

	if ( IsFirstTimePredicted() ) then

		self:DoExtinguish( 196, 1 )

		if ( SERVER ) then

			if ( self.Owner:KeyPressed( IN_ATTACK ) || !self.Sound ) then
				--self:SendWeaponAnim( ACT_VM_PRIMARYATTACK )

				self.Sound = CreateSound( self.Owner, Sound( "weapons/extinguisher/fire1.wav" ) )
				
				self:Idle()
			end

			if ( self:Ammo1() > 0 ) then if ( self.Sound ) then self.Sound:Play() end end

		end
	end

	if SERVER then
		local trace = {}
		trace.start = self.Owner:GetShootPos()
		trace.endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * 150
		trace.filter = self.Owner

		local tr = util.TraceLine( trace )
		
		for k, v in pairs( ents.FindInSphere( tr.HitPos, 50 ) ) do
			if v:GetClass() == "fire" then
				v:Hurt(self.Owner, sfs_HoseDamage)
			end
			
			if v:IsOnFire() then 
				v:Extinguish()
				v:SetColor( Color( 255, 255, 255, 255 ) )
			end
		end
	end

		local ammoAdd = 0
		if ( self.Owner:WaterLevel() > 1 ) then ammoAdd = 0 end
		for k, v in pairs(ents.FindInSphere( self.Owner:GetPos(), 500)) do
			if v:GetClass() == "hydrant" then
				ammoAdd = 10
			end
			if v:GetClass() == "prop_vehicle_jeep" then
				for i, j in pairs(sfs_FireTrucks) do
					if (v:GetVehicleClass() == j) then
						ammoAdd = 10
					end
				end
			end
		end

		self:SetNextPrimaryFire( CurTime() + 0.05 )

		if (ammoAdd == 0) then
			DarkRP.notify(self.Owner, 1, 4, "You Need To Be Close To A Fire Truck Or Fire Hydrant To Use The Fire Hose")
			self.Owner:SetAmmo(0, self:GetPrimaryAmmoType())
			self:SetNextPrimaryFire( CurTime() + 1 )
		end
end

function SWEP:SecondaryAttack()
end

function SWEP:Reload()
end

function SWEP:PlaySound()
	self:EmitSound( "weapons/extinguisher/release1.wav", 100, math.random( 95, 110 ) )
end

function SWEP:Think()

	if ( self:GetNextIdle() > 0 && CurTime() > self:GetNextIdle() ) then

		self:DoIdleAnimation()
		self:Idle()

	end

	if ( self:GetNextSecondaryFire() > CurTime() || CLIENT ) then return end
	
	if ( ( self.NextAmmoReplenish or 0 ) < CurTime() ) then
		local ammoAdd = 0
		if ( self.Owner:WaterLevel() > 1 ) then ammoAdd = 0 end
		for k, v in pairs(ents.FindInSphere( self.Owner:GetPos(), 500)) do
			if v:GetClass() == "hydrant" then
				ammoAdd = 10
			end
			if v:GetClass() == "prop_vehicle_jeep" then
				for i, j in pairs(sfs_FireTrucks) do
					if (v:GetVehicleClass() == j) then
						ammoAdd = 10
					end
				end
			end
		end
	
		self.Owner:SetAmmo( math.min( self:Ammo1() + ammoAdd, self.Primary.DefaultClip ), self:GetPrimaryAmmoType() )
		self.NextAmmoReplenish = CurTime() + 0.1
	end

	if ( self.Sound && self.Sound:IsPlaying() && self:Ammo1() < 1 ) then
		self.Sound:Stop()
		self:PlaySound()
		self:DoIdleAnimation()
		self:Idle()
	end

	if ( self.Owner:KeyReleased( IN_ATTACK ) || ( !self.Owner:KeyDown( IN_ATTACK ) && self.Sound ) ) then
	
		self:SendWeaponAnim( ACT_VM_SECONDARYATTACK )

		if ( self.Sound ) then
			self.Sound:Stop()
			self.Sound = nil
			if ( self:Ammo1() > 0 ) then
				self:PlaySound()
				if ( !game.SinglePlayer() ) then self:CallOnClient( "PlaySound", "" ) end
			end
		end

		self:SetNextPrimaryFire( CurTime() + self:SequenceDuration() )
		self:SetNextSecondaryFire( CurTime() + self:SequenceDuration() )
		
		self:Idle()

	end
end

function SWEP:Holster( weapon )
	if ( CLIENT ) then return end

	if ( self.Sound ) then
		self.Sound:Stop()
		self.Sound = nil
	end
	
	return true
end

function SWEP:DoIdleAnimation()
	if ( self.Owner:KeyDown( IN_ATTACK ) && self:Ammo1() > 0 ) then self:SendWeaponAnim( ACT_VM_IDLE_1 ) return end
	if ( self.Owner:KeyDown( IN_ATTACK ) && self:Ammo1() < 1 ) then self:SendWeaponAnim( ACT_VM_IDLE_EMPTY ) return end
	self:SendWeaponAnim( ACT_VM_IDLE )
end

function SWEP:Idle()

	self:SetNextIdle( CurTime() + self:GetAnimationTime() )

end

function SWEP:GetAnimationTime()
	local time = self:SequenceDuration()
	if ( time == 0 && IsValid( self.Owner ) && !self.Owner:IsNPC() && IsValid( self.Owner:GetViewModel() ) ) then time = self.Owner:GetViewModel():SequenceDuration() end
	return time
end

if ( SERVER ) then return end

SWEP.WepSelectIcon = Material( "rb655_extinguisher_icon.png" )

function SWEP:DrawWeaponSelection( x, y, w, h, a )
	surface.SetDrawColor( 255, 255, 255, a )
	surface.SetMaterial( self.WepSelectIcon )
	
	local size = math.min( w, h ) - 32
	surface.DrawTexturedRect( x + w / 2 - size / 2, y + h * 0.05, size, size )
end